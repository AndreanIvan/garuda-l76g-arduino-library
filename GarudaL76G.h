/**
 ****************************************************************
 * Library Shield Garuda L76G untuk Arduino Mega
 * @brief   : Header file
 * 
 * @author  : Ardianto, A.I.
 * @version : 1.0.0
 * @link    : https://lunar-inovasi.co.id/
 * 
 ****************************************************************
 */

#ifndef GarudaL76G_H
#define GarudaL76G_H

#include "Arduino.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

/**     Pengaturan Log     **/
/*   Pilihan: ON atau OFF   */
#define PROCESS_LOG   OFF   // ON: Menampilkan proses yang terjadi pada device
#define ERROR_LOG     OFF   // ON: Menampilkan error yang terjadi pada device
#define UART_LOG      OFF   // ON: Menampilkan log komunikasi UART ke/dari Shield
#define LORA_LOG      OFF   // ON: Menampilkan log komunikasi LoRa ke/dari Gateway

enum class_type {CLASS_A, CLASS_B, CLASS_C};
enum join_mode {OTAA, ABP};
enum off_on {OFF, ON};
enum input_mode {AUTO, MANUAL};

enum returnCode {
    OK                  = 00,
    JOIN_SUCCESSFUL     = 01,
    JOIN_FAILED         = 02,
    JOIN_TIMED_OUT      = 03,
    JOIN_DC_RESTRICTED  = 04,
    DATA_SENT           = 05,
    OTAA_JOINED         = 06,
    ABP_JOINED          = 07,
    DIGIT_NOT_EVEN      = 10,
    GENERAL_ERROR       = 20,
    WRONG_PARAMETER     = 21,
    PARAM_OUT_OF_RANGE  = 22,
    PARAM_DIGIT_ERROR   = 23,
    NETWORK_NOT_JOINED  = 24,
    DEVICE_BUSY         = 25
};

class GarudaL76G
{
    public:

    /** Library Control Function **/

        GarudaL76G(Stream& Shield_Serial, Stream& Monitor_Serial);
        
        uint8_t sendCommand(String Shield_Command);
        String receiveResponse(void);

        bool waitUntilOK(void);
        bool waitUntilString(String str);

        bool processPrint(String str);
        bool errorPrint(String str);
        bool UARTPrint(String str);
        bool LoRaPrint(String str);

        bool freqValidation(String freq);

    /** General Command **/

        /* Digunakan untuk menampilkan semua parameter penting yang tersimpan */
        String getInfo(void);
        
        /* Digunakan untuk menampilkan log yang lebih lengkap dari respon setelah pengiriman */
        uint8_t setDebug(uint8_t DebugStatus);
        String getDebug(void);

    /** Device Command **/

        /* Digunakan untuk mereset semua pengaturan ke pengaturan default pabrik */
        bool factoryReset(void);
        
        /* Melakukan penghematan daya pada perangkat dengan mengubah ke mode sleep */
        bool sleep(void);

        /* Keluar dari mode sleep */
        bool wakeUp(String anyString);

        /* Baca hardware device EUI */
        String getHardwareDevEUI(void);

        /* Baca versi fimware */
        String getFirmwareVersion(void);

    /** Keys, IDs, and EUIs Management **/

        uint8_t setDevEUI(String DevEUI);
        String getDevEUI(void);

        uint8_t setAppEUI(String AppEUI);
        String getAppEUI();

        uint8_t setAppKey(String AppKey);
        String getAppKey(void);

        uint8_t setDevAddr(String DevAddr);
        String getDevAddr(void);

        uint8_t setNwkSKey(String NwkSKey);
        String getNwkSKey(void);

        uint8_t setAppSKey(String AppSKey);
        String getAppSKey(void);

        uint8_t setMCAddr(String MCAddr);
        String getMCAddr(void);

        uint8_t setMCNwkSKey(String MCNwkSKey);
        String getMCNwkSKey(void);

        uint8_t setMCAppSKey(String MCAppSKey);
        String getMCAppSKey(void);

        uint8_t initOTAA(String DevEUI, String AppEUI, String AppKey);
        uint8_t initABP(String DevAddr, String NwksKey, String AppsKey);

    /** Joining, Sending, and Receiving Data on LoRa Network **/

        uint8_t setMode(uint8_t joinMode);
        String getMode(void);

        uint8_t join(void);

        uint8_t getJoinStatus(void);

        uint8_t setPort(uint8_t selectedPort);
        String getPort(void);

        uint8_t setTxConfirm(uint8_t TxConfirm);
        String getTxConfirm(void);

        byte getLastRSSI(void);
        byte getLastSNR(void);

        uint8_t sendData(String DataSend);
        String receiveData(void);
        String getInfoData(void);

        class getInfoClass{
            public:
                String paramValue(String paramName, String completeString, int indexAdd = 0, String pivotStr = "");
                uint8_t stringParsing(String dataString);
                String joinMode(void);
                String adaptiveDR(void);
                uint8_t dataRate(void);
                uint8_t powerIndex(void);
                uint8_t txPower(void);                
                String txConfirm(void);
                uint8_t portSelected(void);
                String debugStatus(void);
                String devEUI(void);
                String appEUI(void);
                String appKey(void);
                String devAddr(void);
                String nwkSKey(void);
                String appSKey(void);

            private:
                uint8_t _joinMode       = -1;
                uint8_t _adaptiveDR     = -1;
                uint8_t _dataRate       = -1;
                uint8_t _powerIndex     = -1;
                uint8_t _txPower        = -1;
                uint8_t _txConfirm      = -1;
                uint8_t _portSelected   = -1;
                uint8_t _debugStatus    = -1;
                String _devEUI          = "0000000000000000";
                String _appEUI          = "0000000000000000";
                String _appKey          = "00000000000000000000000000000000";
                String _devAddr         = "00000000";
                String _nwkSKey         = "00000000000000000000000000000000";
                String _appSKey         = "00000000000000000000000000000000";
        } info;

        class receivedStringClass{
            public:
                String paramValue(String paramName, String completeString, int spaceCount);
                uint8_t stringParsing(String dataString);
                uint8_t port(void);
                String payload(void);
                uint16_t rxCounter(void);
                String rxWindow(void);
                int RSSI(void);
                int SNR(void);
                String macCommand(void);
                String payloadParseToString(int index, int length);
                long payloadParseToInt(int index, int length);
            
            private:
                uint8_t _port = 0;
                String _payload = "";
                uint16_t _rxCounter = 0;
                String _rxWindow = "";
                int _RSSI = 0;
                int _SNR = 0;
                String _macCommand = "";
        } receivedData;

        

    /** LoRa Network Management **/

        uint8_t setClass(uint8_t loraClass)
        String getClass(void)

        uint8_t setADR(uint8_t ADRStat);
        String getADR(void);

        uint8_t setDataRate(uint8_t dataRate);
        String getDataRate(void);

        uint8_t setRx2(uint8_t dataRate, String chFreq);
        String getRx2(void);

        uint8_t setTxPower(byte power_dBm);
        String getTxPower(void);
        uint8_t setPowerIndex(byte power_index);
        String getPowerIndex(void);

        uint8_t setChannelFrequency(uint8_t channelID, String chFreq);
        String getChannelFrequency(char channelID);

        uint8_t setJoinChannelStatus(uint8_t channelID, uint8_t chState);
        String getJoinChannelStatus(uint8_t channelID);

        String getJoinChannelMask(void);

        uint8_t setChannelStatus(uint8_t channelID, uint8_t chState);
        String getChannelStatus(uint8_t channelID);
        
        String getChannelMask(void);

        uint8_t setChannelDataRateRange(uint8_t channelID, uint8_t DRMin, uint8_t DRMax);
        String getChannelDataRateRange(uint8_t channelID);

    /** Command for Testing **/

        bool startTxTest(void);
        bool stopTxTest(void);

    /********************************************************************************/

    private:

    Stream& _Shield_Serial;
    Stream& _Monitor_Serial;
    uint8_t _joinMode;
    uint8_t _TxConfirm;
    String _DevAddr = "";
    String _DevEUI = "";
    String _AppEUI = "";
    String _NwkSKey = "";
    String _AppKey = "";
    String _AppSKey = "";
    String _MCAddr = "";
    String _MCNwkSKey = "";
    String _MCAppSKey = "";

};

#endif
