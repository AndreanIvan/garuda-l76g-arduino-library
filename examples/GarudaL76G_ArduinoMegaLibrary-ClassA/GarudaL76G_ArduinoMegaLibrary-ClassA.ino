/**************************************************
 *  Contoh Penggunaan Library untuk Garuda-L76G   *
 *        ----- Loop Program Class A -----        *
 **************************************************/
#include <GarudaL76G.h>

/** Pengaturan Join Mode **/
/*  Pilihan: OTAA, ABP    */
#define JOIN_MODE         OTAA

/** Pengaturan Jaringan LoRa **/
/*  Pilihan ADR_STATUS: ON, OFF */
#define ADR_STATUS        OFF
/*  Jika ADR OFF, perlu diatur DATA_RATE dan TX_POWER */
#if ADR_STATUS == OFF
  #define DATA_RATE       5         // 0-5
  #define TX_POWER        16        // 0-16 (dBm), bernilai genap
#endif
/*  Pengaturan Port */
#define LORAWAN_PORT      1         // 1-255, selain 224 & 240
/*  Pengaturan TX Confirm */
#define TX_CONFIRM        OFF        // ON atau OFF
/*  Pengaturan Debug Mode */
#define DEBUG_MODE        ON        // ON atau OFF
/*  Pengaturan Input Pengiriman Data */
#define DATA_INPUT_MODE   MANUAL    // AUTO atau MANUAL

/** Pengaturan Secure Element Identity **/
/* Jika menggunakan Join Mode OTAA */
  String DevEUI  = "00a1b2c3d4e5f600";
  String AppEUI  = "0000000000000000";
  String AppKey  = "0afb313ccff7ccdc2fc5f7b55d815cba";
/* Jika menggunakan Join Mode ABP */
  String DevAddr = "00000000";
  String NwksKey = "00000000000000000000000000000000";
  String AppsKey = "00000000000000000000000000000000";

/** Pengaturan Serial **/
#define DebugSerial     Serial
#define GarudaSerial    Serial1
/*  Baud Rate dan Time Out */
#define DEBUG_BAUD_RATE 115200
#define L76G_BAUD_RATE  115200
#define DEBUG_TIME_OUT  200
#define L76G_TIME_OUT   200

/** Deklarasi Objek Serial **/
GarudaL76G GarudaShield(GarudaSerial, DebugSerial);

/** Deklarasi Fungsi **/
void deviceBanner(void);
void deviceIntro(void);
void deviceJoin(void);
void deviceSend(String dataTerima);
void joinRetry(void);

/** Setup **/
void setup(){

  DebugSerial.begin(DEBUG_BAUD_RATE);
  GarudaSerial.begin(L76G_BAUD_RATE);

  DebugSerial.setTimeout(DEBUG_TIME_OUT);
  GarudaSerial.setTimeout(L76G_TIME_OUT);

  /* Banner */
  deviceBanner();

  /* Atur Class, ADR, DR, TX Power */
  GarudaShield.setClass(CLASS_A)
  GarudaShield.setADR(ADR_STATUS);
  if (ADR_STATUS == OFF){
    GarudaShield.setDataRate(DATA_RATE);
    GarudaShield.setTxPower(TX_POWER);
  }

  /* Atur Tx Confirm */
  GarudaShield.setTxConfirm(TX_CONFIRM);

  //delay(100);         // !!!

  /* Atur Join Mode */
  GarudaShield.setMode(JOIN_MODE);

  /* Atur Join Parameters */
  switch (JOIN_MODE){
    case OTAA : GarudaShield.initOTAA(DevEUI, AppEUI, AppKey); break;
    case ABP  : GarudaShield.initABP(DevAddr, NwksKey, AppsKey); break;
  }

  /* Atur Debug Mode */
  GarudaShield.setDebug(DEBUG_MODE);

  /* Intro */
  deviceIntro();

  /* Spasi */
  DebugSerial.println("=============================================\n");
  
  /* Join ke dalam jaringan */
  deviceJoin();
}

/** Loop **/
void loop(){
  String dataTerima = "";
  int delaySleep = 5000;

  GarudaShield.setPort(LORAWAN_PORT);

  /* Jika data dari input */
  if (DATA_INPUT_MODE == MANUAL){
    DebugSerial.println("Port: " + (String)LORAWAN_PORT);
    DebugSerial.println("Ketikkan data:");
    while (!(DebugSerial.available()));
    dataTerima = DebugSerial.readStringUntil('\0');
  }
  else
  {
  
    /* Data 1 byte (0 - 255) */
    uint8_t data_1B[4];
    data_1B[0] = 12;
    data_1B[1] = 23;
    data_1B[2] = 123;
    data_1B[3] = 234;
  
    /* Data 2 byte (0 - 65.535) */
    uint16_t data_2B[2];
    data_2B[0] = 12;
    data_2B[1] = 56789;
  
    /* Data float */
    float float_value = 501.333;
    uint8_t data_f[4], data_temp[4];
    *(float*)data_temp = float_value;
    data_f[0] = data_temp[0];
    data_f[1] = data_temp[1];
    data_f[2] = data_temp[2];
    data_f[3] = data_temp[3];
  
    /* Data 4 byte to 1 byte */
    uint32_t data_4B = 1234567890;
    uint8_t data_buff_1B[4];
    data_buff_1B[0] = (data_4B >> 24) & 0xFF;
    data_buff_1B[1] = (data_4B >> 16) & 0xFF;
    data_buff_1B[2] = (data_4B >> 8) & 0xFF;
    data_buff_1B[3] = data_4B & 0xFF;
  
    /* Print data dalam bentuk HEX ke string dengan format: data_1B, data_2B, data_f */
    char temp[100];
    sprintf(temp, "%02x%02x%02x%02x%04x%04x%02x%02x%02x%02x%02x%02x%02x%02x\n\r", data_1B[0], data_1B[1],
      data_1B[2], data_1B[3], data_2B[0], data_2B[1], data_f[0], data_f[1], data_f[2], data_f[3],
      data_buff_1B[0], data_buff_1B[1], data_buff_1B[2], data_buff_1B[3]);
  
    dataTerima = temp;
  }

  //DebugSerial.println(dataTerima);
  dataTerima.trim();

  /* Kirim ke Gateway */
  deviceSend(dataTerima);
  
  DebugSerial.println("Data dikirim: " + dataTerima);
  DebugSerial.println("===========  Sleep selama " + (String)delaySleep + (" ms ==========="));
  GarudaShield.sleep();
  delay(delaySleep);
  GarudaSerial.println();
  DebugSerial.println("=============== Device Bangun ===============");
  delay(1000);
}

/** Definisi Fungsi **/

void deviceBanner(){
  String shieldVer = GarudaShield.getFirmwareVersion();
  DebugSerial.println("=============================================");
  DebugSerial.println("||         Example Class A Program         ||");
  DebugSerial.println("||           Garuda-L76G  Shield           ||");
  DebugSerial.println("||            - FW  v" + shieldVer + " -            ||");
  DebugSerial.println("=============================================");
}

void deviceIntro(){

  GarudaShield.getInfoData();
  
  /* Baca Mode Join */
  DebugSerial.println("Join Mode  : " + GarudaShield.info.joinMode());

  /* Baca ADR */
  DebugSerial.println("ADR Status : " + GarudaShield.info.adaptiveDR());
  if (GarudaShield.info.adaptiveDR() == "OFF"){
    DebugSerial.println("Data Rate  : " + (String)GarudaShield.info.dataRate());
    DebugSerial.println("Tx Power   : " + (String)GarudaShield.info.txPower() + " dBm");
  }

  /* Baca TX Confirm */
  DebugSerial.println("Tx Confirm : " + GarudaShield.info.txConfirm());
  
  /* Baca Parameter SE Identity */
  if (GarudaShield.info.joinMode() == "OTAA"){
    DebugSerial.println("DevEUI     : " + GarudaShield.info.devEUI());
    DebugSerial.println("AppEUI     : " + GarudaShield.info.appEUI());
    DebugSerial.println("AppKey     : " + GarudaShield.info.appKey());
  } else if (GarudaShield.info.joinMode() == "ABP"){
    DebugSerial.println("DevAddr    : " + GarudaShield.info.devAddr());
    DebugSerial.println("NwksKey    : " + GarudaShield.info.nwkSKey());
    DebugSerial.println("AppsKey    : " + GarudaShield.info.appSKey());
  } else
    DebugSerial.println("Mode tidak terdeteksi");
  
}

void deviceJoin(void){
  int joinState = 4;
  while (joinState != JOIN_SUCCESSFUL){
    DebugSerial.println("Mencoba untuk join...");
    joinState = GarudaShield.join();
    switch (joinState){
      case JOIN_SUCCESSFUL    : DebugSerial.println("=============== Join Berhasil ==============="); break;
      case JOIN_FAILED        : DebugSerial.println("================= Join Gagal ================"); break;
      case JOIN_TIMED_OUT     : DebugSerial.println("=============== Join Timed Out =============="); break;
      case JOIN_DC_RESTRICTED : DebugSerial.println("=========== Duty-Cycle Restricted ==========="); break;
      default                 : DebugSerial.println("Status join tidak terdeteksi: " + joinState);
    }
  }
}
void deviceSend(String dataTerima){
  byte sendStatus;
  DebugSerial.println("Mengirim data... ");
  sendStatus = GarudaShield.sendData(dataTerima);

  if (sendStatus == DATA_SENT)
    DebugSerial.println("=============== Data Terkirim ===============");
}
