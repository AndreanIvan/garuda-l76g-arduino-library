/**************************************************
 *  Contoh Penggunaan Library untuk Garuda-L76G   *
 *        ----- Loop Program Class C -----        *
 **************************************************/
#include <GarudaL76G.h>

/** Pengaturan Join Mode **/
/*  Pilihan: OTAA, ABP    */
#define JOIN_MODE     OTAA

/** Pengaturan Data Rate **/
/*  Pilihan ADR_STATUS: ON, OFF */
#define ADR_STATUS  OFF
/*  Jika ADR OFF, perlu diatur DATA_RATE dan TX_POWER */
#if ADR_STATUS == OFF
  #define DATA_RATE   5     // 0-5
  #define TX_POWER    16    // 0-16 (dBm), bernilai genap
#endif
/*  Pengaturan Port */
#define LORAWAN_PORT  1     // 1-255, selain 224 & 240
/*  Pengaturan TX Confirm */
#define TX_CONFIRM    ON
/*  Pengaturan Debug Mode */
#define DEBUG_MODE    ON

/** Pengaturan Secure Element Identity **/
/* Jika menggunakan Join Mode OTAA */
  String DevEUI  = "00a1b2c3d4e5f600";
  String AppEUI  = "0000000000000000";
  String AppKey  = "0afb313ccff7ccdc2fc5f7b55d815cba";
/* Jika menggunakan Join Mode ABP */
  String NwksKey = "00000000000000000000000000000000";
  String AppsKey = "00000000000000000000000000000000";
  String DevAddr = "00000000";

/** Pengaturan Serial **/
#define DebugSerial     Serial
#define GarudaSerial    Serial1
/*  Baud Rate dan Time Out */
#define DEBUG_BAUD_RATE 115200
#define L76G_BAUD_RATE  115200
#define DEBUG_TIME_OUT  200
#define L76G_TIME_OUT   200

/** Deklarasi Objek Serial **/
GarudaL76G GarudaShield(GarudaSerial, DebugSerial);

/** Deklarasi Fungsi **/
void deviceBanner(void);
void deviceIntro(void);
void deviceJoin(void);
void deviceSend(String dataTerima);
void joinRetry(void);

/** Setup **/
void setup(){

  delay(1000);
  
  DebugSerial.begin(DEBUG_BAUD_RATE);
  GarudaSerial.begin(L76G_BAUD_RATE);

  DebugSerial.setTimeout(DEBUG_TIME_OUT);
  GarudaSerial.setTimeout(L76G_TIME_OUT);

  /* Banner */
  deviceBanner();

  /* Atur Class, ADR, DR, TX Power */
  GarudaShield.setClass(CLASS_C)
  GarudaShield.setADR(ADR_STATUS);
  if (ADR_STATUS == OFF){
    GarudaShield.setDataRate(DATA_RATE);
    GarudaShield.setTxPower(TX_POWER);
  }

  /* Atur Tx Confirm */
  GarudaShield.setTxConfirm(TX_CONFIRM);

  //delay(100);         // !!!

  /* Atur Join Mode */
  GarudaShield.setMode(JOIN_MODE);

  /* Atur Join Parameters */
  switch (JOIN_MODE){
    case OTAA : GarudaShield.initOTAA(DevEUI, AppEUI, AppKey); break;
    case ABP  : GarudaShield.initABP(DevAddr, NwksKey, AppsKey); break;
  }

  /* Atur Debug Mode */
  GarudaShield.setDebug(DEBUG_MODE);

  /* Intro */
  deviceIntro();

  /* Spasi */
  DebugSerial.println("=============================================\n");
  
  /* Join ke dalam jaringan */
  deviceJoin();

  /*  Jika menggunakan server Chirpstack, perlu dilakukan 2 kali
   *  pengiriman sebelum server dapat mengirim downlink.
   */
  deviceSend("");
  deviceSend("");
}

/** Loop **/
void loop(){
  String dataTerima = "";
  int payload[3];
  int delaySleep = 2000;

  GarudaShield.setPort(LORAWAN_PORT);

  DebugSerial.println("Menunggu data...");
  
  while (!(GarudaSerial.available()));
  GarudaShield.receiveData();
  dataTerima = GarudaShield.receivedData.payload();
  DebugSerial.println("Payload diterima: " + dataTerima);

  payload[0] = GarudaShield.receivedData.payloadParseToInt(0,1);
  
  digitalWrite(11, (int)dataTerima[0] - 48);
  digitalWrite(12, (int)dataTerima[1] - 48);
  digitalWrite(10, (int)dataTerima[2] - 48);
  
}

/** Definisi Fungsi **/

void deviceBanner(){
  String shieldVer = GarudaShield.getFirmwareVersion();
  DebugSerial.println("=============================================");
  DebugSerial.println("||         Example Class C Program         ||");
  DebugSerial.println("||           Garuda-L76G  Shield           ||");
  DebugSerial.println("||            - FW  v" + shieldVer + " -            ||");
  DebugSerial.println("=============================================");
}

void deviceIntro(){

  GarudaShield.getInfoData();
  
  /* Baca Mode Join */
  DebugSerial.println("Join Mode  : " + GarudaShield.info.joinMode());

  /* Baca ADR */
  DebugSerial.println("ADR Status : " + GarudaShield.info.adaptiveDR());
  if (GarudaShield.info.adaptiveDR() == "OFF"){
    DebugSerial.println("Data Rate  : " + (String)GarudaShield.info.dataRate());
    DebugSerial.println("Tx Power   : " + (String)GarudaShield.info.txPower() + " dBm");
  }

  /* Baca TX Confirm */
  DebugSerial.println("Tx Confirm : " + GarudaShield.info.txConfirm());
  
  /* Baca Parameter SE Identity */
  if (GarudaShield.info.joinMode() == "OTAA"){
    DebugSerial.println("DevEUI     : " + GarudaShield.info.devEUI());
    DebugSerial.println("AppEUI     : " + GarudaShield.info.appEUI());
    DebugSerial.println("AppKey     : " + GarudaShield.info.appKey());
  } else if (GarudaShield.info.joinMode() == "ABP"){
    DebugSerial.println("DevAddr    : " + GarudaShield.info.devAddr());
    DebugSerial.println("NwksKey    : " + GarudaShield.info.nwkSKey());
    DebugSerial.println("AppsKey    : " + GarudaShield.info.appSKey());
  } else
    DebugSerial.println("Mode tidak terdeteksi");
  
}

void deviceJoin(void){
  int joinState = 4;
  while (joinState != JOIN_SUCCESSFUL){
    DebugSerial.println("Mencoba untuk join...");
    joinState = GarudaShield.join();
    switch (joinState){
      case JOIN_SUCCESSFUL    : DebugSerial.println("=============== Join Berhasil ==============="); break;
      case JOIN_FAILED        : DebugSerial.println("================= Join Gagal ================"); break;
      case JOIN_TIMED_OUT     : DebugSerial.println("=============== Join Timed Out =============="); break;
      case JOIN_DC_RESTRICTED : DebugSerial.println("=========== Duty-Cycle Restricted ==========="); break;
      default                 : DebugSerial.println("Status join tidak terdeteksi: " + joinState);
    }
  }
}
void deviceSend(String dataTerima){
  byte sendStatus;
  DebugSerial.println("Mengirim data... ");
  sendStatus = GarudaShield.sendData(dataTerima);

  if (sendStatus == DATA_SENT)
    DebugSerial.println("=============== Data Terkirim ===============");
}
