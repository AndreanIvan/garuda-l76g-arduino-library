/**
 ****************************************************************
 * Library Shield Garuda L76G untuk Arduino Mega
 * @brief   : Main file
 * 
 * @author  : Ardianto, A.I.
 * @version : 1.0.0
 * @link    : https://lunar-inovasi.co.id/
 * 
 ****************************************************************
 */

#include "GarudaL76G.h"

extern "C"
{
    #include "string.h"
    #include "stdlib.h"
}

String Shield_Response = "";
String Shield_Command = "";

/** Library Control Function **/

    GarudaL76G::GarudaL76G(Stream& Shield_Serial, Stream& Monitor_Serial) : _Shield_Serial(Shield_Serial), _Monitor_Serial(Monitor_Serial){
        _Shield_Serial.setTimeout(2000);
        _Monitor_Serial.setTimeout(1000);
    }

    uint8_t GarudaL76G::sendCommand(String Shield_Command){
        /* Baca selama masih ada input dari Arduino */
        while (_Shield_Serial.available()){
            _Shield_Serial.read();
        }
        
        UARTPrint(Shield_Command);             
        _Shield_Serial.println(Shield_Command);
        return true;
    }

    String GarudaL76G::receiveResponse(){
        Shield_Response = _Shield_Serial.readString();
        Shield_Response.trim();
        UARTPrint(Shield_Response);
        return Shield_Response;
    }

    bool GarudaL76G::waitUntilOK(){
    Shield_Response = receiveResponse();
    if (Shield_Response.indexOf("ok") < 0)
        return false;
    else
        return true;
    }

    bool GarudaL76G::waitUntilString(String str){
    Shield_Response = receiveResponse();
    if (Shield_Response.indexOf(str) < 0)
        return false;
    else
        return true;
    }

    bool GarudaL76G::processPrint(String str){
        if (PROCESS_LOG){
            _Monitor_Serial.println(str);
            return true;
        } else
            return false;
    }

    bool GarudaL76G::errorPrint(String str){
        if (PROCESS_LOG || ERROR_LOG){
            _Monitor_Serial.println(str);
            return true;
        } else
            return false;
    }

    bool GarudaL76G::UARTPrint(String str){
        if (UART_LOG && str != ""){
            if (str.indexOf("mac") >= 0)
                _Monitor_Serial.println("\nSEND: \"" + str + "\"");
            else
                _Monitor_Serial.println("RECV: \"" + str + "\"");
            return true;
        } else
            return false;
    }

    bool GarudaL76G::LoRaPrint(String str){
        if (LORA_LOG && str != ""){
            _Monitor_Serial.println(str);
            return true;
        } else
            return false;
    }

    bool GarudaL76G::freqValidation(String freq){
        uint32_t freqVal;
        freqVal = freq.toInt();
        if (freqVal >= 920000000 && freqVal <= 923000000)
            return true;
        else
            return false;
    }

/** General Command **/

    String GarudaL76G::getInfo(void){
        sendCommand("mac get_info");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setDebug(uint8_t DebugStatus)
    {  
        switch (DebugStatus){
            case OFF : sendCommand("mac set_debug off"); break;
            case ON  : sendCommand("mac set_debug on"); break;
            default  :
                errorPrint("Debug state selection must be on/off!");
                return WRONG_PARAMETER;
        }
        waitUntilOK();
        return OK;
    }
    String GarudaL76G::getDebug()
    {
        sendCommand("mac get_debug");
        return receiveResponse();
    }

    bool GarudaL76G::factoryReset()
    {
        sendCommand("mac factory_reset");
        waitUntilOK();
        return OK;
    }

    bool GarudaL76G::sleep()
    {
        sendCommand("mac sleep");
        waitUntilOK();
        return true;
    }

    bool GarudaL76G::wakeUp(String anyString)
    {
        sendCommand(anyString);
        waitUntilOK();
        return true;
    }

    String GarudaL76G::getHardwareDevEUI(void){
        sendCommand("mac get_hwdeveui");
        return receiveResponse();
    }

    String GarudaL76G::getFirmwareVersion(void){
        sendCommand("mac get_ver");
        return receiveResponse();
    }

/** Keys, IDs, and EUIs Management **/

    uint8_t GarudaL76G::setDevEUI(String DevEUI)
    {
        if (DevEUI.length() == 16){
            _DevEUI = DevEUI;
            sendCommand("mac set_deveui " + DevEUI);
            waitUntilOK();
            return OK;
        }
        else {
            errorPrint("Parameter DevEUI is set incorrectly!");
            return PARAM_DIGIT_ERROR;
        }
    }
    String GarudaL76G::getDevEUI()
    {
        sendCommand("mac get_deveui");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setAppEUI(String AppEUI)
    {
        if (AppEUI.length() == 16){
            _AppEUI = AppEUI;
            sendCommand("mac set_appeui " + AppEUI);
            waitUntilOK();
            return OK;
        }
        else {
            errorPrint("Parameter AppEUI is set incorrectly!");
            return PARAM_DIGIT_ERROR;
        }
    }
    String GarudaL76G::getAppEUI()
    {
        sendCommand("mac get_appeui");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setAppKey(String AppKey)
    {
        if (AppKey.length() == 32){
            _AppKey = AppKey;
            sendCommand("mac set_appkey " + AppKey);
            waitUntilOK();
            return OK;
        }
        else {
            errorPrint("Parameter AppKey is set incorrectly!");
            return PARAM_DIGIT_ERROR;
        }

    }
    String GarudaL76G::getAppKey()
    {
        sendCommand("mac get_appkey");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setDevAddr(String DevAddr)
    {
        if (DevAddr.length() == 8){
            _DevAddr = DevAddr;
            sendCommand("mac set_devaddr " + DevAddr);
            waitUntilOK();
            return OK;
        }
        else {
            errorPrint("Parameter DevAddr is set incorrectly!");
            return PARAM_DIGIT_ERROR;
        }
    }
    String GarudaL76G::getDevAddr()
    {
        sendCommand("mac get_devaddr");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setNwkSKey(String NwkSKey)
    {
        if (NwkSKey.length() == 32){
            _NwkSKey = NwkSKey;
            sendCommand("mac set_nwkskey " + NwkSKey);
            waitUntilOK();
            return OK;
        }
        else {
            errorPrint("Parameter NwksKey is set incorrectly!");
            return PARAM_DIGIT_ERROR;
        }
    }
    String GarudaL76G::getNwkSKey()
    {
        sendCommand("mac get_nwkskey");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setAppSKey(String AppSKey)
    {
        if (AppSKey.length() == 32){
            _AppSKey = AppSKey;
            sendCommand("mac get_appskey " + AppSKey);
            waitUntilOK();
            return OK;
        }
        else {
            errorPrint("Parameter AppsKey is set incorrectly!");
            return PARAM_DIGIT_ERROR;
        }
    }
    String GarudaL76G::getAppSKey()
    {
        sendCommand("mac get_appskey");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setMCAddr(String MCAddr)
    {
        if (MCAddr.length() == 8){
            _MCAddr = MCAddr;
            sendCommand("mac set_mcaddr " + MCAddr);
            waitUntilOK();
            return OK;
        }
        else {
            errorPrint("Parameter MCAddr is set incorrectly!");
            return PARAM_DIGIT_ERROR;
        }
    }
    String GarudaL76G::getMCAddr(void)
    {
        sendCommand("mac get_mcaddr");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setMCNwkSKey(String MCNwkSKey)
    {
        if (MCNwkSKey.length() == 32){
            _MCNwkSKey = MCNwkSKey;
            sendCommand("mac set_mcnwkskey " + MCNwkSKey);
            waitUntilOK();
            return OK;
        }
        else {
            errorPrint("Parameter MCNwkSKey is set incorrectly!");
            return PARAM_DIGIT_ERROR;
        }
    }
    String GarudaL76G::getMCNwkSKey(void)
    {
        sendCommand("mac get_mcnwkskey");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setMCAppSKey(String MCAppSKey)
    {
        if (MCAppSKey.length() == 32){
            _MCAppSKey = MCAppSKey;
            sendCommand("mac set_mcappskey " + MCAppSKey);
            waitUntilOK();
            return OK;
        }
        else {
            errorPrint("Parameter MCAppSKey is set incorrectly!");
            return PARAM_DIGIT_ERROR;
        }
    }
    String GarudaL76G::getMCAppSKey(void)
    {
        sendCommand("mac get_mcappskey");
        return receiveResponse();
    }

/** Joining, Sending, and Receiving Data on LoRa Network **/

    uint8_t GarudaL76G::setMode(uint8_t joinMode) // enum
    {
        switch (joinMode){
            case OTAA : sendCommand("mac set_mode otaa"); break;
            case ABP  : sendCommand("mac set_mode abp"); break;
            default   : 
                errorPrint("Mode selection must be OTAA or ABP!");
                return WRONG_PARAMETER;
                break;
        }
        _joinMode = joinMode;
        waitUntilOK();
        return OK;
    }
    String GarudaL76G::getMode()
    {
        sendCommand("mac get_mode");
        return receiveResponse();
    }

    uint8_t GarudaL76G::join()
    {
        String rec = "";
        bool joinStatus;

        sendCommand("mac join");

        while ((rec.indexOf("otaa") < 0) && (rec.indexOf("abp") < 0)){
            rec = _Shield_Serial.readString();
            rec.trim();
            LoRaPrint(rec);
        }
        
        if (rec.indexOf("joined") >= 0){
            processPrint("Join successful!");
            return JOIN_SUCCESSFUL;
        } else
        if (rec.indexOf("join fail") >= 0){
            processPrint("Join failed!");
            return JOIN_FAILED;
        } else
        if (rec.indexOf("dutycycle restricted") >= 0){
            processPrint("Duty cycle restricted, too many trials!");
            return JOIN_DC_RESTRICTED;
        } else
            processPrint("Join timed out!");
            return JOIN_TIMED_OUT;
    }

    uint8_t GarudaL76G::getJoinStatus(void){
        String resp = "";
        
        sendCommand("mac get_nwksstatus");

        if (resp.indexOf("otaa joined") >= 0){
            processPrint("Joined by OTAA.");
            return OTAA_JOINED;
        } else
        if (resp.indexOf("abp joined") >= 0){
            processPrint("Joined by ABP.");
            return ABP_JOINED;
        } else
        if (resp.indexOf("no network joined yet") >= 0){
            processPrint("Not joined yet!");
            return NETWORK_NOT_JOINED;
        } else {
            errorPrint("Wrong response from device.");
            return GENERAL_ERROR;
        }
    }

    uint8_t GarudaL76G::setPort(uint8_t selectedPort)
    {
        if((selectedPort >= 0) && (selectedPort <= 255))
            if((selectedPort != 224) && (selectedPort != 240)){
                sendCommand("mac set_port " + (String)selectedPort);
                waitUntilOK();
                return OK;
            } else {
                errorPrint("Port not allowed!");
                return WRONG_PARAMETER;
            }
        else {
            errorPrint("Port number out of range!");
            return PARAM_OUT_OF_RANGE;
        }
    }
    String GarudaL76G::getPort()
    {
        sendCommand("mac get_port");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setTxConfirm(uint8_t TxConfirm)
    {
        _TxConfirm = TxConfirm;
        switch (TxConfirm){
            case OFF : sendCommand("mac set_txconfirm off"); break;
            case ON  : sendCommand("mac set_txconfirm on"); break;
            default  : 
                errorPrint("TxConfirm selection must be on/off!");
                return WRONG_PARAMETER;
        }
        waitUntilOK();
        return OK;
    }
    String GarudaL76G::getTxConfirm()
    {
        sendCommand("mac get_txconfirm");
        return receiveResponse();
    }

    byte GarudaL76G::getLastRSSI(void){
        String RSSI;

        sendCommand("mac get_rssi");
        RSSI = receiveResponse();
        RSSI.trim();

        return RSSI.toInt();
    }
    byte GarudaL76G::getLastSNR(void){
        String SNR;

        sendCommand("mac get_snr");
        SNR = receiveResponse();
        SNR.trim();

        return SNR.toInt();
    }

    uint8_t GarudaL76G::sendData(String DataSend)
    {
        String sendResp = "";
        
        _Shield_Serial.println("mac send " + DataSend);
        
        if (_joinMode == OTAA && _TxConfirm)
            while (sendResp.indexOf("rx ok") < 0){
                sendResp = _Shield_Serial.readString();
                LoRaPrint(sendResp);
            }
        else 
            while (sendResp.indexOf("tx ok") < 0){
                sendResp = _Shield_Serial.readString();
                LoRaPrint(sendResp);
            }
        processPrint("Data Sent!");
        return DATA_SENT;
    }

    String GarudaL76G::receiveData(){
        String response = "";
        
        while (!(_Shield_Serial.available()));
        response = receiveResponse();
        LoRaPrint(response);

        receivedData.stringParsing(response);
        return response;
    }

    String GarudaL76G::getInfoData(){
        String getInfoResp = "";

        getInfoResp = getInfo();
        info.stringParsing(getInfoResp);


        return getInfoResp;
    }

    /** Get Info Class **/
    String GarudaL76G::getInfoClass::paramValue(String paramName, String completeString, int indexAdd = 0, String pivotStr = ""){
        String value = "";
        int stringIndex = completeString.indexOf(paramName);
        int paramIndex = stringIndex + paramName.length() + indexAdd + 1;

        if (pivotStr != "")
            paramIndex = completeString.indexOf(pivotStr, stringIndex) + 2;
        
        while (completeString[paramIndex] != ' ' && completeString[paramIndex] != '\n'){
            value += completeString[paramIndex];
            paramIndex++;
        }


        return value;
    }
    uint8_t GarudaL76G::getInfoClass::stringParsing(String dataInfo){
        

        if (dataInfo.indexOf("otaa") >= 0)
            _joinMode = OTAA;
        else
            _joinMode = ABP;
        
        
        if (dataInfo.indexOf("adr on") >= 0)
            _adaptiveDR = ON;
        else
            _adaptiveDR = OFF;


        String strDataRate = paramValue("dr :", dataInfo);
        _dataRate = strDataRate.toInt();


        String strPowerIndex = paramValue("txpow", dataInfo, 2);
        _powerIndex = strPowerIndex.toInt();


        if (dataInfo.indexOf("unconfirmed") >= 0)
            _txConfirm = OFF;
        else
            _txConfirm = ON;


        String strPort = paramValue("port", dataInfo);
        _portSelected = strPort.toInt();


        if (dataInfo.indexOf("debug on") >= 0)
            _debugStatus = ON;
        else
            _debugStatus = OFF;


        _devEUI = paramValue("deveui", dataInfo, 0, ":");
        _appEUI = paramValue("appeui", dataInfo, 0, ":");
        _appKey = paramValue("appkey", dataInfo, 0, ":");
        _devAddr = paramValue("devaddr", dataInfo, 0, ":");
        _nwkSKey = paramValue("nwkskey", dataInfo, 0, ":");
        _appSKey = paramValue("appskey", dataInfo, 0, ":");

        return OK;
    }
    String GarudaL76G::getInfoClass::joinMode(void){
        switch (_joinMode){
            case OTAA : return "OTAA"; break;
            case ABP  : return "ABP"; break;
            default: return "Not set up yet"; break;
        }
    }
    String GarudaL76G::getInfoClass::adaptiveDR(void){
        switch (_adaptiveDR){
            case OFF : return "OFF"; break;
            case ON  : return "ON"; break;
            default: return "Not set up yet"; break;
        }
    }
    uint8_t GarudaL76G::getInfoClass::dataRate(void){
        return _dataRate;
    }
    uint8_t GarudaL76G::getInfoClass::powerIndex(void){
        return _powerIndex;
    }
    uint8_t GarudaL76G::getInfoClass::txPower(void){
        return _powerIndex * (-2) + 16;
    }
    String GarudaL76G::getInfoClass::txConfirm(void){
        switch (_txConfirm){
            case OFF : return "OFF"; break;
            case ON  : return "ON"; break;
            default: return "Not set up yet"; break;
        }
    }
    uint8_t GarudaL76G::getInfoClass::portSelected(void){
        return _portSelected;
    }
    String GarudaL76G::getInfoClass::debugStatus(void){
        switch (_debugStatus){
            case OFF : return "OFF"; break;
            case ON  : return "ON"; break;
            default: return "Not set up yet"; break;
        }
    }
    String GarudaL76G::getInfoClass::devEUI(void){
        return _devEUI;
    }
    String GarudaL76G::getInfoClass::appEUI(void){
        return _appEUI;
    }
    String GarudaL76G::getInfoClass::appKey(void){
        return _appKey;
    }
    String GarudaL76G::getInfoClass::devAddr(void){
        return _devAddr;
    }
    String GarudaL76G::getInfoClass::nwkSKey(void){
        return _nwkSKey;
    }
    String GarudaL76G::getInfoClass::appSKey(void){
        return _appSKey;
    }


    /** Received String Class **/

    String GarudaL76G::receivedStringClass::paramValue(String paramName, String completeString, int spaceCount = 0){
        String value = "";
        int stringIndex = completeString.indexOf(paramName);
        int paramIndex = stringIndex + paramName.length() + 1;

        if (spaceCount != 0){
            int spaceCounter = 0;
            int indexCounter = 0;
            while (spaceCounter < spaceCount){
                if (completeString[indexCounter] == ' ') spaceCounter++;
                indexCounter++;
            }
            paramIndex = indexCounter;
        }
        
        while (completeString[paramIndex] != ' ' && completeString[paramIndex] != '\n'){
            value += completeString[paramIndex];
            paramIndex++;
        }

        return value;
    }
    uint8_t GarudaL76G::receivedStringClass::stringParsing(String dataString){
        String strPort = paramValue("port", dataString);
        _port = strPort.toInt();

        _payload = paramValue("rx 1", dataString, 2);

        String strCounter = paramValue("rx counter", dataString);
        _rxCounter = strCounter.toInt();

        _rxWindow = paramValue("rx window", dataString);

        String strRSSI = paramValue("rx rssi", dataString);
        _RSSI = strRSSI.toInt();

        String strSNR = paramValue("rx snr", dataString);
        _SNR = strSNR.toInt();

        _macCommand = paramValue("mac command", dataString);

        return OK;
    }
    String GarudaL76G::receivedStringClass::payloadParseToString(int dataIndex, int dataLength){
        String parsedString = "";
        
        for (int i = dataIndex; i < (dataIndex + dataLength); i++)
            parsedString += _payload[i];

        return parsedString;
    }
    long GarudaL76G::receivedStringClass::payloadParseToInt(int dataIndex, int dataLength){
        long parsedInt;
        String parsedString = "";
        char *hexString = "";
       
        for (int i = dataIndex; i < (dataIndex + dataLength); i++){
            parsedString += _payload[i];
        }

        hexString = parsedString.c_str();
        
        parsedInt = strtol(&hexString[0], '\n', 16);
        
        return parsedInt;
    }
    uint8_t GarudaL76G::receivedStringClass::port(){
        return _port;
    }
    String GarudaL76G::receivedStringClass::payload(){
        return _payload;
    }
    uint16_t GarudaL76G::receivedStringClass::rxCounter(){
        return _rxCounter;
    }
    String GarudaL76G::receivedStringClass::rxWindow(){
        return _rxWindow;
    }
    int GarudaL76G::receivedStringClass::RSSI(){
        return _RSSI;
    }
    int GarudaL76G::receivedStringClass::SNR(){
        return _SNR;
    }
    String GarudaL76G::receivedStringClass::macCommand(){
        return _macCommand;
    }

/** LoRa Network Management **/

    uint8_t GarudaL76G::setClass(uint8_t loraClass) // enum
    {
        switch (loraClass){
            case CLASS_A : sendCommand("mac set_class a"); break;
            case CLASS_B : sendCommand("mac set_class b"); break;
            case CLASS_C : sendCommand("mac set_class c"); break;
            default :
                errorPrint("Class selection must be A or C");
                return WRONG_PARAMETER;
        }
        waitUntilOK();
        return OK;
    }
    String GarudaL76G::getClass()
    {
        sendCommand("mac get_class");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setADR(uint8_t ADRStat) // enum
    {
        switch (ADRStat){
            case OFF : sendCommand("mac set_adr off"); break;
            case ON  : sendCommand("mac set_adr on"); break;
            default  : 
                errorPrint("ADR selection must be on/off!");
                return WRONG_PARAMETER;
        }
        waitUntilOK();
        return OK;
    }
    String GarudaL76G::getADR()
    {
        sendCommand("mac get_adr");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setDataRate(uint8_t dataRate)
    {
        if (dataRate >= 0 && dataRate <= 5){
            sendCommand("mac set_dr " + (String)dataRate);
            waitUntilOK();
            return OK;
        } else {
            errorPrint("Data Rate must be between 0 to 5!");
            return PARAM_OUT_OF_RANGE;
        }
    }
    String GarudaL76G::getDataRate()
    {
        sendCommand("mac get_dr");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setRx2(uint8_t dataRate, String chFreq)
    {
        if (dataRate >= 0 && dataRate <= 5){
            if (freqValidation(chFreq)){
                sendCommand("mac set_rx2 " + (String)dataRate + ' ' + chFreq);
                waitUntilOK();
                return OK;
            } else {
                errorPrint("Frequency not allowed! (Must be between 920 - 923 MHz)");
                return WRONG_PARAMETER;
            }
        } else {
            errorPrint("Data rate must be between 0 to 5!");
            return PARAM_OUT_OF_RANGE;
        }
    }
    String GarudaL76G::getRx2()
    {
        sendCommand("mac get_rx2");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setTxPower(uint8_t power_dBm)
    {
        if (power_dBm >= 0 && power_dBm <= 18){
            if (power_dBm % 2 == 0){
                sendCommand("mac set_txpower " + (String)power_dBm);
                waitUntilOK();
                return OK;
            } else {
                errorPrint("Tx Power must be an even value!");
                return WRONG_PARAMETER;
            }
        } else {
            errorPrint("Tx Power must be between 0 to 16 dBm!");
            return PARAM_OUT_OF_RANGE;
        }
    }
    String GarudaL76G::getTxPower()
    {
        sendCommand("mac get_txpower");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setPowerIndex(uint8_t power_index)
    {
        if (power_index >= 0 && power_index <= 7){
            sendCommand("mac set_pwrindex " + (String)power_index);
            waitUntilOK();
            return OK;
        } else {
            errorPrint("Power Index must be between 0 to 7!");
            return PARAM_OUT_OF_RANGE;
        }
    }
    String GarudaL76G::getPowerIndex()
    {
        sendCommand("mac get_pwrindex");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setChannelFrequency(uint8_t channelID, String chFreq)
    {
        if (channelID >= 0 && channelID <= 16){
            if (freqValidation(chFreq)){
                sendCommand("mac set_chfreq " + (String)channelID + ' ' + chFreq);
                waitUntilOK();
                return OK;
            } else {
                errorPrint("Frequency not allowed! (Must be between 920 - 923 MHz)");
                return WRONG_PARAMETER;
            }
        } else {
            errorPrint("Channel number must be between 0 to 15!");
            return PARAM_OUT_OF_RANGE;
            }
    }
    String GarudaL76G::getChannelFrequency(char channelID)
    {
        if (channelID >= 0 && channelID <= 16){
            sendCommand("mac get_chfreq" + (String)channelID);
            return receiveResponse();
        } else {
            errorPrint("Channel number must be between 0 to 15!");
            return "";
        }
    }

    uint8_t GarudaL76G::setJoinChannelStatus(uint8_t channelID, uint8_t chState)
    {
        if (channelID >= 0 && channelID <= 15){
            switch (chState){
                case OFF : sendCommand("mac set_defchstat " + (String)channelID + " on"); break;
                case ON  : sendCommand("mac set_defchstat " + (String)channelID + " off"); break;
                default  : 
                    errorPrint("Join Channel status selection must be on/off!");
                    return WRONG_PARAMETER;
            }
            waitUntilOK();
            return OK;
        } else {
            errorPrint("Channel number must be between 0 to 15!");
            return false;
        }
    }
    String GarudaL76G::getJoinChannelStatus(uint8_t channelID)
    {
        sendCommand("mac get_defchstat" + (String)channelID);
        return receiveResponse();
    }

    String GarudaL76G::getJoinChannelMask(void)
    {
        sendCommand("mac get_defchmask");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setChannelStatus(uint8_t channelID, uint8_t chState)
    {
        if (channelID >= 0 && channelID <= 15){
            switch (chState){
                case OFF : sendCommand("mac set_chstat " + (String)channelID + " on"); break;
                case ON  : sendCommand("mac set_chstat " + (String)channelID + " off"); break;
                default  : 
                    errorPrint("Channel status selection must be on/off!");
                    return WRONG_PARAMETER;
            }
            waitUntilOK();
            return OK;
        } else {
            errorPrint("Channel number must be between 0 to 15!");
            return false;
        }
    }
    String GarudaL76G::getChannelStatus(uint8_t channelID)
    {
        if (channelID >= 0 && channelID <= 16){
            sendCommand("mac get_chstatus" + (String)channelID);
            return receiveResponse();
        } else {
            errorPrint("Channel number must be between 0 to 15!");
            return "";
        }
    }

    String GarudaL76G::getChannelMask(void)
    {
        sendCommand("mac get_chmask");
        return receiveResponse();
    }

    uint8_t GarudaL76G::setChannelDataRateRange(uint8_t channelID, uint8_t DRMin, uint8_t DRMax){
        if (channelID < 0 || channelID > 15){
            errorPrint("Channel number must be between 0 to 15!");
            return PARAM_OUT_OF_RANGE;
        } else {
            if (DRMin < 0 || DRMin > 5 || DRMax < 0 || DRMax > 5){
                errorPrint("DR Min and DR Max must be between 2 to 5!");
                return PARAM_OUT_OF_RANGE;
            } else {
                if (DRMin > DRMax){
                    errorPrint("DR Min must not be greater than DR Max!");
                    return WRONG_PARAMETER;
                } else {
                    sendCommand("mac set_chdr " + (String)channelID + " " + (String)DRMin + " " + (String)DRMax);
                    return OK;
                }
            }
        }
    }
    String GarudaL76G::getChannelDataRateRange(uint8_t channelID){
        if (channelID >= 0 && channelID <= 16){
            sendCommand("mac get_chdr" + (String)channelID);
            return receiveResponse();
        } else {
            errorPrint("Channel number must be between 0 to 15!");
            return "";
        }
    }

    uint8_t GarudaL76G::initOTAA(String DevEUI, String AppEUI, String AppKey){
        /* Set parameter satu per satu dan menunggu hingga status OK */
        if(setDevEUI(DevEUI) == OK)
            if(setAppEUI(AppEUI) == OK)
                if(setAppKey(AppKey) == OK)
                    return true;
        return false;
    }

    uint8_t GarudaL76G::initABP(String DevAddr, String NwksKey, String AppsKey){
        /* Set parameter satu per satu dan menunggu hingga status OK */
        if(setDevAddr(DevAddr) == OK)
            if(setNwksKey(NwksKey) == OK)
                if(setAppsKey(AppsKey) == OK)
                    return true;
        return false;
    }

/** Command for Testing **/

    bool GarudaL76G::startTxTest()
    {
        sendCommand("mac start_tx_test");
        waitUntilOK();
        return OK;
    }
    bool GarudaL76G::stopTxTest()
    {
        sendCommand("mac stop_tx_test");
        waitUntilOK();
        return OK;
    }